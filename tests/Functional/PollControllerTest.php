<?php

namespace App\Tests\Functional;

use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PollControllerTest extends WebTestCase {
  use FixturesTrait;

  // Test getting all polls
  public function testGetAllPolls() {
    $client = static::createClient();

    $this->loadFixtures(array(
        'App\DataFixtures\AppPollFixtures'
    ));

    $client->request('GET', '/api/v1/poll/');

    $response = $client->getResponse();
    $this->assertEquals(200, $response->getStatusCode());
    $body = $response->getContent();
    $json = json_decode($body, true);
    $this->assertEquals(4, count($json['poll']));
  }

  // Test getting one poll (we should try to check all poll property)
  public function testGetOnePoll() {
    $client = static::createClient();

    $this->loadFixtures(array(
        'App\DataFixtures\AppPollFixtures'
    ));

    $client->request('GET', '/api/v1/poll/1');

    $response = $client->getResponse();
    $this->assertEquals(200, $response->getStatusCode());
    $body = $response->getContent();
    $json = json_decode($body, true);
    $this->assertEquals("citron ou orange", $json['poll']['title']);
  }

  // Test Put some data on a poll
  public function testPutOnePoll() {
    $client = static::createClient();

    $this->loadFixtures(array(
        'App\DataFixtures\AppPollFixtures'
    ));

    $client->request('PUT', '/api/v1/poll/1/notTheToken', [
        'body' => "{
                    \"id\": 1,
                    \"title\": \"Fromage ou dessert ? \",
                    \"description\": \"Votre plat préféré\",
                    \"creation_date\": \"2048-04-25T16:19:48+02:00\",
                    \"expiracy_date\": \"2048-04-25T16:19:48+02:00\",
                    \"kind\": \"text\",
                    \"allowed_answers\": [
                        \"yes\"
                    ],
                    \"modification_policy\": \"nobody\",
                    \"mail_on_vote\": true,
                    \"choices\": [
                        {
                            \"id\": 1,
                            \"name\": \"fromage\"
                        },
                        {
                            \"id\": 2,
                            \"name\": \"dessert\"
                        }
                    ],
                    \"default_expiracy_days_from_now\": 365
                }"
    ]);
    $response = $client->getResponse();
    $this->assertEquals(403, $response->getStatusCode());

    //Same request but with admin key
    $client->request('PUT', '/api/v1/poll/1/5eb5ade73ec4f', [
        'body' => "{
                    \"id\": 1,
                    \"title\": \"Fromage ou dessert ? \",
                    \"description\": \"Votre plat préféré\",
                    \"creation_date\": \"2048-04-25T16:19:48+02:00\",
                    \"expiracy_date\": \"2048-04-25T16:19:48+02:00\",
                    \"kind\": \"text\",
                    \"allowed_answers\": [
                        \"yes\"
                    ],
                    \"modification_policy\": \"nobody\",
                    \"mail_on_vote\": true,
                    \"choices\": [
                        {
                            \"id\": 1,
                            \"name\": \"fromage\"
                        },
                        {
                            \"id\": 2,
                            \"name\": \"dessert\"
                        }
                    ],
                    \"default_expiracy_days_from_now\": 365
                }"
    ]);

    $response = $client->getResponse();
    $this->assertEquals(200, $response->getStatusCode());

    //Checking the result of modification
    $client->request('GET', '/api/v1/poll/1');

    $response = $client->getResponse();
    $this->assertEquals(200, $response->getStatusCode());
    $body = $response->getContent();
    $json = json_decode($body, true);
    $this->assertEquals("Fromage ou dessert ?", $json['poll']['title']);

  }

  // Test Post new poll
  public function testPostOnePoll() {
    $client = static::createClient();

    $this->loadFixtures(array(
        'App\DataFixtures\AppPollFixtures'
    ));

    $data = [
        "title" => "Fromage ou dessert ? ",
        "description" => "Votre plat préféré",
        "creation_date" => "2048-04-25T16:19:48+02:00",
        "expiracy_date" => "2048-04-25T16:19:48+02:00",
        "kind" => "text",
        "allowed_answers" => [
            "yes"
        ],
        "modification_policy" => "nobody",
        "mail_on_vote" => true,
        "choices" => [
            [
                "id" => 1,
                "name" => "fromage"
            ],
            [
                "id" => 2,
                "name" => "dessert"
            ]
        ],
        "default_expiracy_days_from_now" => 365
    ];

    $client->request('POST', '/api/v1/poll/', [
        'json' => $data
    ], [], [], json_encode($data));

    $response = $client->getResponse();
    $this->assertEquals(201, $response->getStatusCode());

    $data = $response->getContent();
    $this->assertIsNumeric($data['poll']['id']);
  }

}
