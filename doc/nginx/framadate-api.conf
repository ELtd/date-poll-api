
server {
    listen 80;
    listen [::]:80;
    server_name framadate-api.cipherbliss.com;
    # enforce https
    return 301 https://$server_name$request_uri;

}
server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name framadate-api.cipherbliss.com;

    # Use Mozilla's guidelines for SSL/TLS settings
    # https://mozilla.github.io/server-side-tls/ssl-config-generator/

    # Path to the root of your installation
		root /home/www/tykayn/cipherbliss/framadate/;

		location / {
        # try to serve file directly, fallback to index.html to see the frontend of funky framadate
        try_files $uri /index.html$is_args$args;

        # handle OPTIONS requests
        # @note: don't try to DRY out this "if" block, or you're gonna have a bad time.
        # @see: http://wiki.nginx.org/IfIsEvil
        if ($request_method = 'OPTIONS') {
          add_header 'Access-Control-Allow-Credentials' 'true';
          add_header 'Access-Control-Allow-Headers'     'Authorization,Content-Type,Accept,Origin,User-Agent,DNT,Cache-Control,X-Mx-ReqToken,Keep-Alive,X-Requested-With,If-Modified-Since';
          add_header 'Access-Control-Allow-Methods'     'GET, DELETE, OPTIONS, POST, PUT';
          add_header 'Access-Control-Allow-Origin'      'https://framadate-api.cipherbliss.com';
          add_header 'Access-Control-Max-Age'           2592000;
          add_header 'Content-Length'                   0;
          add_header 'Content-Type'                     'text/plain charset=UTF-8';
          return 204;
        }
        # send the CORS headers
        add_header 'Access-Control-Allow-Credentials' 'true';
        add_header 'Access-Control-Allow-Origin'      'https://framadate-api.cipherbliss.com';

        # set additional security headers
        add_header 'Cache-Control'                    'no-cache, no-store, must-revalidate';
        add_header 'Content-Security-Policy'          'connect-src framadate-api.cipherbliss.com';
        add_header 'Expires'                          '0';
        add_header 'Pragma'                           'no-cache';
        add_header 'Strict-Transport-Security'        'max-age=31536000; includeSubDomains';
        add_header 'X-Content-Type-Options'           'nosniff';
        add_header 'X-Frame-Options'                  'DENY';
        add_header 'X-XSS-Protection'                 '1; mode=block';
    }

    location = /favicon.ico {
            log_not_found off;
            access_log off;
    }

    location = /robots.txt {
            allow all;
            log_not_found off;
            access_log off;
    }


    location ~* \.(png|jpg|jpeg|gif|ico)$ {
            expires max;
            log_not_found off;
    }

       # PROD
    location ~ ^/app\.php(/|$) {
        include fastcgi.conf;
        fastcgi_intercept_errors on;
        fastcgi_pass php-handler;
       # When you are using symlinks to link the document root to the
       # current version of your application, you should pass the real
       # application path instead of the path to the symlink to PHP
       # FPM.
       # Otherwise, PHP's OPcache may not properly detect changes to
       # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
       # for more information).
       fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
       fastcgi_param DOCUMENT_ROOT $realpath_root;
       # Prevents URIs that include the front controller. This will 404:
       # http://domain.tld/app.php/some-path
       # Remove the internal directive to allow URIs like this
       internal;
   }

   # return 404 for all other php files not matching the front controller
   # this prevents access to other php files you don't want to be accessible.
   location ~ \.php$ {
     return 404;
   }
}
