#Database setup

In the mysql prompt, to create a new user and a dedicated database, run this:
```
CREATE DATABASE IF NOT EXISTS framadate_api;
CREATE USER 'framadate-admin'@'localhost' IDENTIFIED BY 'framadate-admin-password';
GRANT ALL PRIVILEGES ON framadate_api.* TO 'framadate-admin'@'localhost';
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'framadate-admin'@'localhost';
SHOW DATABASES;
```

now you have your config ready to host Framadate API.
