<?php

namespace App\Form;

use App\Entity\Poll;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PollType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('customUrl')
            ->add('description')
            ->add('creationDate')
            ->add('expiracyDate')
            ->add('kind')
            ->add('allowedAnswers')
            ->add('modificationPolicy')
            ->add('mailOnComment')
            ->add('mailOnVote')
            ->add('hideResults')
            ->add('showResultEvenIfPasswords')
            ->add('password')
            ->add('adminKey')
            ->add('owner')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Poll::class,
        ]);
    }
}
