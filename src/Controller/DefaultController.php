<?php

namespace App\Controller;

use App\Entity\Owner;
use App\Entity\Poll;
use App\Repository\PollRepository;
use App\Service\MailService;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Route;
use JMS\Serializer\Type\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

/**
 * Class DefaultController
 * @package App\Controller
 * @Route("/",name="home_")
 */
class DefaultController extends FramadateController {

	/**
	 * @Get(path ="/",
	 *      name = "sweet_home")
	 */
	public function indexAction() {

		$polls = $this->getDoctrine()->getRepository( Poll::class )->findAll();

		return $this->render( 'pages/home.html.twig',[ ]);
	}


}
