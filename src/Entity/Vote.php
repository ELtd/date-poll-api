<?php

	namespace App\Entity;

	use DateTimeInterface;
	use Doctrine\ORM\Mapping as ORM;
	use JMS\Serializer\Annotation as Serializer;

	/**
	 * @ORM\Entity(repositoryClass="App\Repository\VoteRepository")
   * @Serializer\ExclusionPolicy("all")
	 */
	class Vote {
		/**
		 * for a text kind of choice: could be "yes" "no" "maybe" and empty.
		 * for a date kind, the choice linked is equivalent to the value selected
		 * @ORM\Column(type="string", length=255, nullable=true)
		 * @Serializer\Type("string")
		 * @Serializer\Expose()
		 */
		public $value;
		/**
		 * @ORM\Column(type="datetime" , options={"default"="CURRENT_TIMESTAMP"})
		 * @Serializer\Type("datetime")
		 * @Serializer\Expose()
		 */
		public $creationDate;
		/**
		 * @ORM\ManyToOne(targetEntity="App\Entity\Choice", inversedBy="votes", cascade={"persist"})
		 * @ORM\JoinColumn(nullable=false)
		 * @Serializer\Type("App\Entity\choice")
		 * @Serializer\Expose()
		 */
		public $choice;
		/**
		 * @ORM\Id()
		 * @ORM\GeneratedValue()
		 * @ORM\Column(type="integer")
		 * @Serializer\Type("integer")
		 * @Serializer\Expose()
		 */
		private $id;
		/**
		 * @ORM\ManyToOne(targetEntity="App\Entity\Poll", inversedBy="votes", cascade={"persist"})
		 * @ORM\JoinColumn(nullable=false)
		 */
		private $poll;
		/**
		 * @ORM\ManyToOne(targetEntity="App\Entity\StackOfVotes", inversedBy="votes", cascade={"persist"})
		 * @ORM\JoinColumn(nullable=false)
		 * @Serializer\Type("App\Entity\StackOfVotes")
		 */
		private $stacksOfVotes;

		public function display() {
			return [
					'id'        => $this->getId(),
					'value'     => $this->getValue(),
					'choice_id' => $this->getChoice()->getId(),
					'text'      => $this->getChoice()->getName(),
			];
		}

		public function __construct() {
			$this->setCreationDate( new \DateTime() );
		}

		public function getId(): ?int {
			return $this->id;
		}

		public function getPoll(): ?Poll {
			return $this->poll;
		}

		public function setPoll( ?Poll $poll ): self {
			$this->poll = $poll;
			if ( $poll ) {
				$poll->addVote( $this );
			}

			return $this;
		}

		public function getChoice(): ?Choice {
			return $this->choice;
		}

		public function setChoice( ?Choice $choice ): self {
			$this->choice = $choice;

			return $this;
		}

		public function getValue(): ?string {
			return $this->value;
		}

		public function setValue( ?string $value ): self {
			$this->value = $value;

			return $this;
		}

		public function getCreationDate(): ?DateTimeInterface {
			return $this->creationDate;
		}

		public function setCreationDate( DateTimeInterface $creationDate ): self {
			$this->creationDate = $creationDate;

			return $this;
		}

		public function getStacksOfVotes(): ?StackOfVotes {
			return $this->stacksOfVotes;
		}

		public function setStacksOfVotes( ?StackOfVotes $stacksOfVotes ): self {
			$this->stacksOfVotes = $stacksOfVotes;

			return $this;
		}
	}
