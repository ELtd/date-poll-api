<?php

	namespace App\Entity;

	use DateTime;
	use DateTimeInterface;
	use Doctrine\Common\Collections\ArrayCollection;
	use Doctrine\Common\Collections\Collection;
	use Doctrine\ORM\Mapping as ORM;
	use JMS\Serializer\Annotation as Serializer;

	/**
	 * one poll choice, could be a text or a date
	 * @ORM\Entity(repositoryClass="App\Repository\ChoiceRepository")
	 * @Serializer\ExclusionPolicy("all")
	 */
	class Choice {
		/**
		 * @ORM\Id()
		 * @ORM\GeneratedValue()
		 * @ORM\Column(type="integer")
		 * @Serializer\Expose()
		 */
		public $id;

		/**
		 * @ORM\Column(type="string", length=255, nullable=true)
		 * @Serializer\Type("string")
		 * @Serializer\Expose()
		 */
		public $name;
		/**
		 * @ORM\Column(type="string", length=1024, nullable=true)
		 * @Serializer\Type("string")
		 * @Serializer\Expose()
		 */
		public $url;

		/**
		 * @ORM\Column(type="datetime", nullable=true)
		 * @Serializer\Type("datetime")
		 */
		public $dateTime;
		/**
		 * @ORM\OneToMany(targetEntity="App\Entity\Vote", mappedBy="choice", cascade={"persist"})
		 * @Serializer\Type("App\Entity\Vote")
		 */
		public $votes;
		/**
		 * @ORM\ManyToOne(targetEntity="App\Entity\Poll", inversedBy="choices", cascade={"persist"})
		 * @Serializer\Type("App\Entity\Poll")
		 */
		private $poll;

		public function __construct( $optionalName = null ) {
			$this->poll  = new ArrayCollection();
			$this->votes = new ArrayCollection();
			$this->setDateTime( new DateTime() );
			if ( $optionalName ) {
				$this->setName( $optionalName );
			}
		}


		public function display() {
			return [
					'id'   => $this->getId(),
					'date' => $this->getDateTime(),
					'text' => $this->getName(),
					'url'  => $this->getUrl(),
			];
		}

		public function getId(): ?int {
			return $this->id;
		}

		public function getDateTime(): ?DateTimeInterface {
			return $this->dateTime;
		}

		public function setDateTime( ?DateTimeInterface $dateTime ): self {
			$this->dateTime = $dateTime;

			return $this;
		}

		public function getName(): ?string {
			return $this->name;
		}

		public function setName( ?string $name ): self {
			$this->name = $name;

			return $this;
		}

		public function getPoll(): ?Poll {
			return $this->poll;
		}

		public function setPoll( ?Poll $poll ): self {
			$this->poll = $poll;

			return $this;
		}

		/**
		 * @return Collection|Vote[]
		 */
		public function getVotes(): Collection {
			return $this->votes;
		}

		public function addVote( Vote $vote ): self {
			if ( ! $this->votes->contains( $vote ) ) {
				$this->votes[] = $vote;
				$vote->setChoice( $this );
			}

			return $this;
		}

		public function removeVote( Vote $vote ): self {
			if ( $this->votes->contains( $vote ) ) {
				$this->votes->removeElement( $vote );
				// set the owning side to null (unless already changed)
				if ( $vote->getChoice() === $this ) {
					$vote->setChoice( null );
				}
			}

			return $this;
		}

		public function getUrl(): ?string {
			return $this->url;
		}

		public function setUrl( ?string $url ): self {
			$this->url = $url;

			return $this;
		}
	}
