<?php

namespace App\Traits;

use Doctrine\ORM\Mapping as ORM;

trait Timed {
	/**
	 * @ORM\Column(type="datetime" , options={"default"="CURRENT_TIMESTAMP"})
	 */
	private $createdAt;
}
