<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * contains the votes for one answer to a poll
 * @ORM\Entity(repositoryClass="App\Repository\StackOfVotesRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("all")
 */
class StackOfVotes {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;
	/**
	 * @ORM\Column(type="string", length=255)
	 * @Serializer\Type("string")
	 * @Serializer\Expose()
	 */
	public $pseudo;
	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Vote", mappedBy="stacksOfVotes", cascade={"persist","remove"})
	 * @Serializer\Expose()
	 */
	public $votes;
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Poll", inversedBy="stacksOfVotes", cascade={"persist"})
	 */
	private $poll;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Owner", inversedBy="stackOfVotes", cascade={"persist"})
	 * @Serializer\Expose()
	 */
	private $owner;


	public function display() {
		$tab = [
			'id'             => $this->getId(),
			'modifier_token' => $this->getOwner()->getModifierToken(),
			'pseudo'         => '',
			'creation_date'  => '',
			'votes'          => [],
		];
		// prefill votes with all choices ids
		foreach ( $this->getPoll()->getChoices() as $choice ) {
			$tab[ 'votes' ][ $choice->getId() ] = [
				'choice_id' => $choice->getId(),
			];
		}

		foreach ( $this->getVotes() as $vote ) {
			$tab[ 'votes' ][ $vote->getChoice()->getId() ]               = $vote->display();
			$tab[ 'votes' ][ $vote->getChoice()->getId() ][ 'stack_id' ] = $this->getId();
			$tab[ 'pseudo' ]                                             = $this->getOwner()->getPseudo();
			$tab[ 'creation_date' ]                                      = $vote->getCreationDate();
		}

		return $tab;
	}

	/**
	 * @ORM\PrePersist
	 */
	public function prePersist() {
		$this->setPseudo( $this->getOwner()->getPseudo() );
	}

	public function __construct() {
		$this->votes = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	/**
	 * @return Collection|poll[]
	 */
	public function getVotes(): Collection {
		return $this->votes;
	}

	public function addVote( Vote $vote ): self {
		if ( ! $this->votes->contains( $vote ) ) {
			$vote->setPoll( $this->getPoll() );

			$this->votes[] = $vote;
			$vote->setStacksOfVotes( $this );
		}

		return $this;
	}

	public function removeVote( Vote $vote ): self {
		if ( $this->votes->contains( $vote ) ) {
			$this->votes->removeElement( $vote );
			// set the owning side to null (unless already changed)
			if ( $vote->getStacksOfVotes() === $this ) {
				$vote->setStacksOfVotes( null );
			}
		}

		return $this;
	}

	public function getPseudo(): ?string {
		return $this->pseudo;
	}

	public function setPseudo( ?string $pseudo ): self {
		$this->pseudo = $pseudo;

		return $this;
	}

	public function getOwner(): ?Owner {
		return $this->owner;
	}

	public function setOwner( ?Owner $owner ): self {
		$this->owner = $owner;

		return $this;
	}

	public function getPoll(): ?Poll {
		return $this->poll;
	}

	public function setPoll( ?Poll $poll ): self {
		$this->poll = $poll;

		return $this;
	}
}
