<?php

	namespace App\Entity;

	use DateTimeInterface;
	use Doctrine\Common\Collections\ArrayCollection;
	use Doctrine\Common\Collections\Collection;
	use Doctrine\ORM\Mapping as ORM;
	use JMS\Serializer\Annotation as Serializer;

	/**
	 * @ORM\Entity(repositoryClass="App\Repository\PollRepository")
   * @Serializer\ExclusionPolicy("all")
	 */
	class Poll {
		/**
		 * @ORM\Id()
		 * @ORM\GeneratedValue()
		 * @ORM\Column(type="integer")
		 * @Serializer\Expose()
		 * @Serializer\Type("integer")
		 */
		public $id;

		/**
		 * @ORM\Column(type="string", length=255)
		 * @Serializer\Expose()
		 * @Serializer\Type("string")
		 */
		public $title;
		/**
		 * @ORM\Column(type="string", length=255, nullable=true)
		 * @Serializer\Expose()
		 * @Serializer\Type("string")
		 */
		public $customUrl;
		/**
		 * @ORM\Column(type="string", length=1000)
		 * @Serializer\Expose()
		 * @Serializer\Type("string")
		 */
		public $description;
		/**
		 * @ORM\Column(type="datetime" , options={"default"="CURRENT_TIMESTAMP"})
		 * @Serializer\Expose()
		 */
		public $creationDate;
		/**
		 * @ORM\Column(type="datetime")
		 * @Serializer\Expose()
		 */
		public $expiracyDate;
		/**
		 * @ORM\ManyToOne(targetEntity="App\Entity\Owner", inversedBy="polls",cascade={"persist"})
		 * @ORM\JoinColumn(nullable=false)
		 * @Serializer\Type("App\Entity\Owner")
		 * @Serializer\Expose()
		 */
		public $owner;
		/**
		 * kind of poll, text or date
		 * @ORM\Column(type="string", length=255)
		 * @Serializer\Type("string")
		 * @Serializer\Expose()
		 */
		public $kind = 'text';
		/**
		 * array of possible answer to each choice, by default: "yes" or nothing only.
		 * could be also "yes", "maybe", "no". extensible to anything
		 * @ORM\Column(type="array")
		 * @Serializer\Type("array")
		 * @Serializer\Expose()
		 */
		public $allowedAnswers;
		/**
		 * kind of way the people can modify the poll
		 * everybody - can modify votes
		 * self - one can only modify its own vote
		 * nobody - no one can modify the votes (excepted admin), pray to have it right at first
		 * @ORM\Column(type="string", length=255)
		 * @Serializer\Type("string")
		 * @Serializer\Expose()
		 */
		public $modificationPolicy = 'nobody';
		/**
		 * send a mail on a new comment
		 * @ORM\Column(type="boolean", nullable=true)
		 * @Serializer\Type("boolean")
		 * @Serializer\Expose()
		 */
		public $mailOnComment;
		/**
		 * send a mail on a new vote
		 * @ORM\Column(type="boolean", nullable=true)
		 * @Serializer\Type("boolean")
		 * @Serializer\Expose()
		 */
		public $mailOnVote;
		/**
		 * hide publicly results
		 * @ORM\Column(type="boolean", nullable=true)
		 * @Serializer\Type("boolean")
		 * @Serializer\Expose()
		 */
		public $hideResults;
		/**
		 * show publicly results even if there is a password to access the vote
		 * @ORM\Column(type="boolean", nullable=true)
		 * @Serializer\Type("boolean")
		 * @Serializer\Expose()
		 */
		public $showResultEvenIfPasswords;
		/**
		 * @ORM\OneToMany(targetEntity="App\Entity\Vote", mappedBy="poll", orphanRemoval=true, cascade={"persist", "remove"})
		 * @Serializer\Type("ArrayCollection<App\Entity\Vote>")
		 * @Serializer\Expose()
		 */
		public $votes;
		/**
		 * @ORM\OneToMany(targetEntity="App\Entity\StackOfVotes", mappedBy="poll", orphanRemoval=true, cascade={"persist", "remove"})
		 * @Serializer\Expose()
		 */
		public $stacksOfVotes;
		/**
		 * @ORM\OneToMany(targetEntity="App\Entity\Choice", mappedBy="poll", orphanRemoval=true, cascade={"persist", "remove"})
		 * @Serializer\Expose()
     * @Serializer\Type("ArrayCollection<App\Entity\Choice>")
		 */
		public $choices;
		/**
		 * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="poll", orphanRemoval=true, cascade={"persist", "remove"})
		 * @Serializer\Expose()
		 * @Serializer\Type("ArrayCollection<App\Entity\Comment>")
		 */
		public $comments;
		/**
		 * vote restricted by a password in md5 format
		 * @ORM\Column(type="string", length=255, nullable=true)
		 */
		private $password;
		/**
		 * used to allow administration
		 * @ORM\Column(type="string", length=255)
		 * @Serializer\Type("string")
		 */
		private $adminKey;

		/**
		 * number of days from now for default expiracy date
		 * @var int
		 * @Serializer\Expose()
		 */
		public $defaultExpiracyDaysFromNow = 60;
		private $maxChoicesLimit = 25;

		public function computeAnswers() {
			// counts each number of answer for this choice
			$computedArray = [];
			$maxScore      = 0;

			foreach ( $this->getStacksOfVotes() as $stack_of_vote ) {
				foreach ( $stack_of_vote->getVotes() as $vote ) {
					$answer    = $vote->getValue();
					$choice_id = $vote->getChoice()->getId();

					if ( ! isset( $computedArray[ $choice_id ] ) ) {
						$computedArray[ $choice_id ] = [
								'choice_id'   => $choice_id,
								'choice_text' => $vote->getChoice()->getName(),
								'id'          => $vote->getId(),
								'score'       => 0,
								'yes'         => [
										'count'  => 0,
										'people' => [],
								],
								'maybe'       => [
										'count'  => 0,
										'people' => [],
								],
								'no'          => [
										'count'  => 0,
										'people' => [],
								],
						];
					}
					$computedArray[ $choice_id ][ $answer ][ 'count' ] ++;
					$computedArray[ $choice_id ][ $answer ][ 'people' ][] = $stack_of_vote->getOwner()->getPseudo();

					if ( $answer == 'yes' ) {
						$computedArray[ $choice_id ][ 'score' ] += 1;
					} elseif ( $answer == 'maybe' ) {
						$computedArray[ $choice_id ][ 'score' ] += 0.5;
					}
					// compare with max value
					if ( $computedArray[ $choice_id ][ 'score' ] > $maxScore ) {
						$maxScore = $computedArray[ $choice_id ][ 'score' ];
					}
				}
			}

			return [
					'counts'   => $computedArray,
					'maxScore' => $maxScore,
			];
		}

		public function display() {
			return [
					'poll'    => $this,
					'answers' => $this->computeAnswers(),
			];
		}

		public function __construct() {
			$this->votes         = new ArrayCollection();
			$this->stacksOfVotes = new ArrayCollection();
			$this->choices       = new ArrayCollection();
			$this->comments      = new ArrayCollection();
			$this->initiate();
		}


		private function initiate() {
			$this->adminKey = $this->generateAdminKey();
			$this->setCreationDate( new \DateTime() );
			$this->setExpiracyDate( $this->addDaysToDate(
					new \DateTime(),
					$this->defaultExpiracyDaysFromNow
			) );
			$this->setAllowedAnswers( [ 'yes' ] );
		}


		public function generateAdminKey() {
			$rand = random_int( PHP_INT_MIN, PHP_INT_MAX );

			return str_shuffle( md5( $rand ) . $rand . $this->random_str() );
		}

		/**
		 * Generate a random string, using a cryptographically secure
		 * pseudorandom number generator (random_int)
		 *
		 * This function uses type hints now (PHP 7+ only), but it was originally
		 * written for PHP 5 as well.
		 *
		 * For PHP 7, random_int is a PHP core function
		 * For PHP 5.x, depends on https://github.com/paragonie/random_compat
		 *
		 * @param int $length How many characters do we want?
		 * @param string $keyspace A string of all possible characters
		 *                         to select from
		 *
		 * @return string
		 */
		public function random_str(
				int $length = 64,
				string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
		): string {
			if ( $length < 1 ) {
				throw new \RangeException( "Length must be a positive integer" );
			}
			$pieces = [];
			$max    = mb_strlen( $keyspace, '8bit' ) - 1;
			for ( $i = 0 ; $i < $length ; ++ $i ) {
				$pieces [] = $keyspace[ random_int( 0, $max ) ];
			}

			return implode( '', $pieces );
		}

		public function findChoiceById( int $id ) {

			$choices = $this->getChoices();
			$counter = 0;
			// there must be something cleaner than this in Doctrine ArrayCollection
			foreach ( $choices as $choice ) {
				$counter ++;
				if ( $counter > $this->maxChoicesLimit ) {
					throw new \ErrorException( "max number of choices reached for this poll" );
				}
				if ( $choice && $choice->getId() == $id ) {
					return $choice;
				}

			}

			return null;
		}

		public function addDaysToDate( \DateTime $date, int $days ) {
			date_add($date, date_interval_create_from_date_string($days . ' days'));
			return $date;
		}

		public function getId(): ?int {
			return $this->id;
		}

		public function getTitle(): ?string {
			return $this->title;
		}

		public function setTitle( string $title ): self {
			$this->title = $title;

			return $this;
		}

		public function getCreationDate(): ?DateTimeInterface {
			return $this->creationDate;
		}

		public function setCreationDate( DateTimeInterface $creationDate ): self {
			$this->creationDate = $creationDate;

			return $this;
		}

		public function setExpiracyDate( DateTimeInterface $expiracyDate ): self {
			$this->expiracyDate = $expiracyDate;

			return $this;
		}

		public function getOwner(): ?Owner {
			return $this->owner;
		}

		public function setOwner( ?Owner $owner ): self {
			$this->owner = $owner;

			return $this;
		}

		/**
		 * @return Collection|Vote[]
		 */
		public function getVotes(): Collection {
			return $this->votes;
		}


		public function getAdminKey(): ?string {
			return $this->adminKey;
		}

		public function setAdminKey( string $adminKey ): self {
			$this->adminKey = $adminKey;

			return $this;
		}

		public function getDescription(): ?string {
			return $this->description;
		}

		public function setDescription( string $description ): self {
			$this->description = $description;

			return $this;
		}

		public function getKind(): ?string {
			return $this->kind;
		}

		public function setKind( string $kind ): self {
			$this->kind = $kind;

			return $this;
		}

		public function getCustomUrl(): ?string {
			return $this->customUrl;
		}

		public function setCustomUrl( string $customUrl ): self {
			$this->customUrl = $customUrl;

			return $this;
		}

		public function getPassword(): ?string {
			return $this->password;
		}

		public function setPassword( string $password ): self {
			$this->password = md5( $password );

			return $this;
		}

		public function getModificationPolicy(): ?string {
			return $this->modificationPolicy;
		}

		public function setModificationPolicy( string $modificationPolicy ): self {
			$this->modificationPolicy = $modificationPolicy;

			return $this;
		}

		public function getMailOnComment(): ?bool {
			return $this->mailOnComment;
		}

		public function setMailOnComment( bool $mailOnComment ): self {
			$this->mailOnComment = $mailOnComment;

			return $this;
		}

		public function getMailOnVote(): ?bool {
			return $this->mailOnVote;
		}

		public function setMailOnVote( bool $mailOnVote ): self {
			$this->mailOnVote = $mailOnVote;

			return $this;
		}

		public function getHideResults(): ?bool {
			return $this->hideResults;
		}

		public function setHideResults( bool $hideResults ): self {
			$this->hideResults = $hideResults;

			return $this;
		}

		public function getShowResultEvenIfPasswords(): ?bool {
			return $this->showResultEvenIfPasswords;
		}

		public function setShowResultEvenIfPasswords( bool $showResultEvenIfPasswords ): self {
			$this->showResultEvenIfPasswords = $showResultEvenIfPasswords;

			return $this;
		}


		/**
		 * @return Collection|Comment[]
		 */
		public function getComments(): Collection {
			return $this->comments;
		}

		public function addComment( Comment $comment ): self {
			if ( ! $this->comments->contains( $comment ) ) {
				$this->comments[] = $comment;
				$comment->setPoll( $this );
			}

			return $this;
		}

		public function removeComment( Comment $comment ): self {
			if ( $this->comments->contains( $comment ) ) {
				$this->comments->removeElement( $comment );
				// set the owning side to null (unless already changed)
				if ( $comment->getPoll() === $this ) {
					$comment->setPoll( null );
				}
			}

			return $this;
		}

		public function getStacksOfVotes() {
			return $this->stacksOfVotes;
		}

		public function setStacksOfVotes( ?StackOfVotes $stacksOfVotes ): self {
			$this->stacksOfVotes = $stacksOfVotes;

			return $this;
		}


		public function addStackOfVote( StackOfVotes $stackOfVote ): self {
			if ( ! $this->stacksOfVotes->contains( $stackOfVote ) ) {
				$this->stacksOfVotes[] = $stackOfVote;
				$stackOfVote->setPoll( $this );
			}

			return $this;
		}

		public function removeStackOfVote( StackOfVotes $stackOfVote ): self {
			if ( $this->stacksOfVotes->contains( $stackOfVote ) ) {
				$this->stacksOfVotes->removeElement( $stackOfVote );
				// set the owning side to null (unless already changed)
				if ( $stackOfVote->getPoll() === $this ) {
					$stackOfVote->setPoll( null );
				}
			}

			return $this;
		}

		public function getExpiracyDate(): ?\DateTimeInterface {
			return $this->expiracyDate;
		}

		public function addVote( Vote $vote ): self {
			if ( ! $this->votes->contains( $vote ) ) {
				$this->votes[] = $vote;
				$vote->setPoll( $this );
			}

			return $this;
		}

		public function removeVote( Vote $vote ): self {
			if ( $this->votes->contains( $vote ) ) {
				$this->votes->removeElement( $vote );
				// set the owning side to null (unless already changed)
				if ( $vote->getPoll() === $this ) {
					$vote->setPoll( null );
				}
			}

			return $this;
		}

		/**
		 * @return Collection|Choice[]
		 */
		public function getChoices(): Collection {
			return $this->choices;
		}

		public function addTextChoiceArray( Array $choiceTextArray ): self {
			foreach ( $choiceTextArray as $text ) {
				$newChoice = new Choice();
				$newChoice->setName( $text );
				$this->addChoice( $newChoice );
			}

			return $this;
		}


		public function getAllowedAnswers(): ?array {
			return $this->allowedAnswers;
		}

		public function setAllowedAnswers( array $allowedAnswers ): self {
			$this->allowedAnswers = $allowedAnswers;

			return $this;
		}

		public function addStacksOfVote( StackOfVotes $stacksOfVote ): self {
			if ( ! $this->stacksOfVotes->contains( $stacksOfVote ) ) {
				$this->stacksOfVotes[] = $stacksOfVote;
				$stacksOfVote->setPoll( $this );
			}

			return $this;
		}

		public function removeStacksOfVote( StackOfVotes $stacksOfVote ): self {
			if ( $this->stacksOfVotes->contains( $stacksOfVote ) ) {
				$this->stacksOfVotes->removeElement( $stacksOfVote );
				// set the owning side to null (unless already changed)
				if ( $stacksOfVote->getPoll() === $this ) {
					$stacksOfVote->setPoll( null );
				}
			}

			return $this;
		}

		public function addChoice( Choice $choice ): self {
			if ( ! is_null( $this->choices ) ) {
				if ( ! $this->choices->contains( $choice ) ) {
					$this->choices[] = $choice;
					$choice->setPoll( $this );
				}
			} else {
				$this->choices[] = $choice;
				$choice->setPoll( $this );
			}


			return $this;
		}

		public function removeChoice( Choice $choice ): self {
			if ( $this->choices->contains( $choice ) ) {
				$this->choices->removeElement( $choice );
				// set the owning side to null (unless already changed)
				if ( $choice->getPoll() === $this ) {
					$choice->setPoll( null );
				}
			}

			return $this;
		}
	}
